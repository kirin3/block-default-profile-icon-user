// この文字列を画像のURLに含んでいたら初期アイコンと判断する。
var blockImageFile = "default_profile_images";

setInterval(function () {
    addClassToDefaultUserContainer();
}, 5000);

function addClassToDefaultUserContainer() {
    // すべてのアバターノードを取得
    const allAvatarNodeList = document.getElementsByClassName('avatar');

    // filterを使うためにキャスト
    const allAvatarArray = [].map.call(allAvatarNodeList, (element) => {
        return element;
    });

    // 画像にblockImageFileの文字列を含むノードだけを抽出
    const defaultAvatarArray = allAvatarArray.filter((element) => {
        return ~element.src.indexOf(blockImageFile);
    });

    [].map.call(defaultAvatarArray, (element) => {

        // imageから4階層上が1ツイートブロック
        var content = element.parentNode.parentNode.parentNode.parentNode;

        // TODO: 設定からブロック方法を変えられるようにする。
        content.classList.add("bdpiu-block-alpha");
    });
    console.log("Task Complete.");
}